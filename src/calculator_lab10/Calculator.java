/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculator_lab10;

/**
 *
 * @author Rachit Vyas
 */
public class Calculator {
    {
    protected double operand1;
    protected double operand2;
    
    protected Calculator(double operand1, double operand2)
    {
        this.operand1 = operand1;
        this.operand2 = operand2;
    }
    
    public abstract double calculate();
}

        
